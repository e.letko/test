const express = require('express');
const app = express();
const PORT = 3000;
app.get('/', (req, res) => {
  res.send('Hello, World!');
});
app.listen(PORT, () => {
  console.log(`Server running at: http://localhost:${PORT}/`);
});

//BTC price. Endpoint #1
const request = require('request');
request('https://api.coindesk.com/v1/bpi/currentprice.json', function (error, response, body) {
  console.error('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  const obj = JSON.parse(body);
  console.log(obj);
  app.get('/btc', (req, res) => {
    res.send(`Price of BTC is ${obj.bpi.EUR.rate_float}`);
  });
});

//Country capital. Endpoint #2
const fs = require('fs');
const countryJson = fs.readFileSync('country.json');
const countryObj = JSON.parse(countryJson);
app.get(`/capital/country=:country`, (req, res) => {
  let findCountry = countryObj.find(function(findCountry) {
    return findCountry.country === req.params.country
  });
  res.send(`Capital of ${findCountry.country} is ${findCountry.city}`);
}); // Country name shout start with uppercase letter

//Excel SUM. Endpoint #3
const excelToJson = require('convert-excel-to-json');
const result = excelToJson({
    sourceFile: 'data.xlsx'
});
let dataArray = result.Sheet1.map(a => a.A);
console.log(dataArray);
let sum = 0;
for(let i = 0; i < dataArray.length; i++) {
  sum += dataArray[i];
}

app.get('/excel-sum', (req, res) => {
  res.send(`SUM is ${sum}`);
})
app.post('/excel-sum', (req, res) => {
  res.send(`SUM is ${sum}`);
}) //POST request use may be wrong

